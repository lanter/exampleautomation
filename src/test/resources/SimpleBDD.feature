@ExampleTestCategory
Feature: Example Project - Feature - Saying hello world

 // Scenario: Printing hello world in the console
   // Given I am executing a simple script
    //When I add "X" to "Y"
    //Then result is "Z"

  Scenario Outline: Add & Check
    Given I am executing a simple script
    When I add "<X>" to "<Y>"
    Then result is "<Z>"

    Examples:
    | X | Y | Z |
    | 1 | 2 | 3 |
    | 4 | 5 | 9 |



 