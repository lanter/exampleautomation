Feature: API test steps

  Scenario: basic GET request
    When I send a GET request to "http://localhost:63001/manage/info"
    Then response code is 200

  Scenario: basic POST request
    When I send a POST request to "http://test.com" with body "this is a test"
    Then response code is 302
