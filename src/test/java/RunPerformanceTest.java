import cucumber.api.CucumberOptions;
import cucumber.api.perf.CucumberPerf;
import cucumber.api.perf.CucumberPerfOptions;



@CucumberOptions(
        plugin = {"html:target/cucumber-htmlreport", "junit:target/junit-report.xml", "cucumber.formatter.NullFormatter", "null_summary"}, //
        features = {"src/test/resources"}
)
@CucumberPerfOptions(
        //plugin = {"summary_text:file://target/summary"},
        plans = {"src/test/resources/plan"}
        //,tags = {"@performancetest"}
)
public class RunPerformanceTest {


    public static void main(final String args[]) {
        CucumberPerf perf = new CucumberPerf(RunPerformanceTest.class);
        try {
            perf.runThreads();
        } catch (Throwable e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println("Total Ran: " + perf.getTotalRanCount());
        System.out.println("RunTime: " + perf.getRunTime());
        System.exit(0);
    }
}