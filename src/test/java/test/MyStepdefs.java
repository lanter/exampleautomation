package test;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import exampleapis.ExampleHTTPHelper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class MyStepdefs {

    private int result=0;

    @Given("^I am executing a simple script$")
    public void iAmExecutingASimpleScript() {
        System.out.println("Hello World");
    }

    @When("I add {string} to {string}")
    public void iAddTo(String X, String Y) {
        result = Integer.parseInt(X) + Integer.parseInt(Y);
    }

    @Then("result is {string}")
    public void resultIs(String Z) {
        Assert.assertEquals(result, Integer.parseInt(Z));
    }


    private WebDriver driver = null;

    @Before
    public void setup() {

    }

    @Given("I open {string}")
    public void iOpen(String url) {
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.navigate().to(url);
    }

    @When("I type {string}")
    public void iType(String text) {
        String xpath = "//*[name='q']";
        WebElement field = driver.findElement(By.xpath(xpath));
        field.sendKeys(text);
    }

    @And("I click {string}")
    public void iClick(String xpath) {
        WebElement button = driver.findElement(By.xpath(xpath));
        button.click();
    }

    @Then("{string} displays")
    public void displays(String text) {

    }

    @After
    public void cleanup(){

        if (driver != null){
            driver.close();
        }
    }

    private HttpResponse response = null;
    private String responseBody = null;

    @Given("I send a GET request to {string}")
    public void iSendAGETRequestTo(String url) {

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        responseBody = ExampleHTTPHelper.getBody(response);

    }

    @Then("response code is {int}")
    public void responseCodeIs(int responseCode) {

        Assert.assertEquals(responseCode, response.getStatusLine().getStatusCode());
    }

    @When("I send a POST request to {string} with body {string}")
    public void iSendAPOSTRequestTo(String url, String body) {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        try {
            post.setEntity(new StringEntity(body));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        response = null;
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        responseBody = ExampleHTTPHelper.getBody(response);
    }
}
